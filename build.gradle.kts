plugins {
    kotlin("multiplatform") version "1.5.30"
}
repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val guiVistaGroupId = "io.gitlab.gui-vista"
    val guiVistaVer = "0.5.0"
    linuxX64 {
        compilations.getByName("main") {
            val headerDir = "/usr/include"
            cinterops.create("glib2") {
                includeDirs("/usr/lib/x86_64-linux-gnu/glib-2.0/include", "$headerDir/glib-2.0",
                    "$headerDir/gdk-pixbuf-2.0")
            }
            cinterops.create("gio2")
            cinterops.create("gtk3") {
                includeDirs(
                    "$headerDir/gdk-pixbuf-2.0",
                    "$headerDir/harfbuzz",
                    "/usr/lib/x86_64-linux-gnu/glib-2.0/include",
                    "$headerDir/glib-2.0",
                    "$headerDir/atk-1.0",
                    "$headerDir/cairo",
                    "$headerDir/pango-1.0",
                    "$headerDir/gtk-3.0"
                )
            }

            dependencies {
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaVer")
            }
        }
        binaries {
            executable {
                entryPoint = "org.example.kpad.main"
            }
        }
    }
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            val baseDir = "/mnt/pi_image"
            val headerDir = "$baseDir/usr/include"
            cinterops.create("glib2") {
                includeDirs("$baseDir/usr/lib/arm-linux-gnueabihf/glib-2.0/include", "$headerDir/glib-2.0",
                    "$headerDir/gdk-pixbuf-2.0")
            }
            cinterops.create("gio2")
            cinterops.create("gtk3") {
                includeDirs(
                    "$headerDir/gtk-3.0/gdk",
                    "$headerDir/gdk-pixbuf-2.0",
                    "$headerDir/harfbuzz",
                    "$baseDir/usr/lib/arm-linux-gnueabihf/glib-2.0/include",
                    "$headerDir/glib-2.0",
                    "$headerDir/atk-1.0",
                    "$headerDir/cairo",
                    "$headerDir/pango-1.0",
                    "$headerDir/gtk-3.0"
                )
            }

            dependencies {
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaVer")
            }
        }
        binaries {
            executable {
                entryPoint = "org.example.kpad.main"
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.5.30"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaVer")
            }
        }
    }
}

tasks.create("createLinuxX64CBindings") {
    dependsOn("cinteropGlib2LinuxX64", "cinteropGio2LinuxX64", "cinteropGtk3LinuxX64")
}

tasks.create("createLinuxArm32CBindings") {
    dependsOn("cinteropGlib2LinuxArm32", "cinteropGio2LinuxArm32", "cinteropGtk3LinuxArm32")
}
