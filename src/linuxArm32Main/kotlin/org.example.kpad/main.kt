package org.example.kpad

import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application
import org.example.kpad.Controller.appIcon

fun main() {
    guiApplicationActivateHandler = ::activateApplication
    GuiApplication.create(id = "org.example.kpad").use {
        Controller.mainWin = MainWindow(this)
        assignDefaultActivateHandler()
        println("Application Status: ${run()}")
    }
}

@Suppress("UNUSED_PARAMETER")
private fun activateApplication(app: Application) {
    println("Application ID: ${app.appId}")
    Controller.mainWin.createUi {
        icon = appIcon
        changeDefaultSize(width = 600, height = 400)
        title = "KPad"
        visible = true
    }
    Controller.setupMainWindowEvents()
}
