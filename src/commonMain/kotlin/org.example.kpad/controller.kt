package org.example.kpad

import io.gitlab.guiVista.gui.ImageBuffer

internal expect object Controller {
    val iconDir: String
    val appIcon: ImageBuffer?
}
