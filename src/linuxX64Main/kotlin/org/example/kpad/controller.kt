package org.example.kpad

import glib2.FALSE
import gtk3.GDK_CONTROL_MASK
import gtk3.GTK_RESPONSE_ACCEPT
import gtk3.GTK_RESPONSE_CANCEL
import gtk3.GtkFileChooserAction
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.dialog.aboutDialog
import io.gitlab.guiVista.gui.dialog.fileChooserDialog
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.runOnBackgroundThread
import io.gitlab.guiVista.gui.runOnUiThread
import io.gitlab.guiVista.gui.text.TextBuffer
import io.gitlab.guiVista.gui.text.TextBufferIterator
import io.gitlab.guiVista.gui.window.WindowBase
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import platform.posix.getenv
import kotlin.native.concurrent.AtomicReference
import kotlin.native.concurrent.freeze
import kotlin.system.exitProcess

@Suppress("ObjectPropertyName")
private val _filePath = AtomicReference("")
@Suppress("ObjectPropertyName")
private val _txtBuffer = AtomicReference("")

@ThreadLocal
internal actual object Controller {
    internal lateinit var mainWin: MainWindow
    val filePath: String
        get() = _filePath.value
    var txtBuffer: String
        get() = _txtBuffer.value
        set(value) {
            _txtBuffer.value = value
        }
    actual val iconDir = "${getenv("HOME")?.toKString() ?: ""}/Pictures"
    actual val appIcon = ImageBuffer.fromFile("$iconDir/text_editor_32.png")

    fun textFromTextBuffer(buffer: TextBuffer): String {
        val start = TextBufferIterator()
        val end = TextBufferIterator()
        buffer.fetchStartIterator(start)
        buffer.fetchEndIterator(end)
        return buffer.fetchText(start = start, end = end, includeHiddenChars = false)
    }

    fun showAboutDialog() {
        val dialog = aboutDialog {
            logo = appIcon
            programName = "KPad"
            comments = "A desktop application written using Kotlin & GUI Vista"
        }
        dialog.run()
        dialog.close()
    }

    fun showOpenDialog(parent: WindowBase) {
        val dialog = createOpenDialog(parent)
        val resp = dialog.run()
        if (resp == GTK_RESPONSE_ACCEPT) {
            _filePath.value = dialog.fetchFileName().freeze()
            mainWin.updateStatusBar("Opening ${_filePath.value}...")
            runOnBackgroundThread(staticCFunction(::runOpenFileTask))
        }
        dialog.close()
    }

    private fun createOpenDialog(parent: WindowBase) = fileChooserDialog(
        parent = parent,
        title = "Open File",
        firstButtonText = "gtk-cancel",
        firstButtonResponseId = GTK_RESPONSE_CANCEL,
        action = GtkFileChooserAction.GTK_FILE_CHOOSER_ACTION_OPEN
    ) {
        addButton("gtk-open", GTK_RESPONSE_ACCEPT)
    }

    fun showSaveDialog(parent: WindowBase, buffer: TextBuffer) {
        val dialog = createSaveDialog(parent)
        val resp = dialog.run()
        if (resp == GTK_RESPONSE_ACCEPT) {
            _filePath.value = dialog.fetchFileName().freeze()
            mainWin.updateStatusBar("Saving ${_filePath.value}...")
            _txtBuffer.value = textFromTextBuffer(buffer).freeze()
            runOnBackgroundThread(staticCFunction(::runSaveFileTask))
        }
        dialog.close()
    }

    private fun createSaveDialog(parent: WindowBase) = fileChooserDialog(
        parent = parent,
        title = "Save File",
        firstButtonText = "gtk-cancel",
        firstButtonResponseId = GTK_RESPONSE_CANCEL,
        action = GtkFileChooserAction.GTK_FILE_CHOOSER_ACTION_SAVE
    ) {
        addButton("gtk-save", GTK_RESPONSE_ACCEPT)
    }

    fun newFile() {
        with(mainWin) {
            title = "KPad"
            buffer.changeText("")
            _filePath.value = ""
            updateStatusBar("Ready")
            resetFocus()
        }
    }

    fun setupMainWindowEvents() {
        val accelGroup = AcceleratorGroup.create().apply {
            addShortcut(accelMod = GDK_CONTROL_MASK, accelKey = 'o',
                eventHandler = staticCFunction(::openFileKeyPressed))
            addShortcut(accelMod = GDK_CONTROL_MASK, accelKey = 's',
                eventHandler = staticCFunction(::saveFileKeyPressed))
            addShortcut(accelMod = GDK_CONTROL_MASK, accelKey = 'n',
                eventHandler = staticCFunction(::newFileKeyPressed))
            addShortcut(accelMod = GDK_CONTROL_MASK, accelKey = 'q',
                eventHandler = staticCFunction(::quitKeyPressed))
        }
        mainWin.addAccelGroup(accelGroup)
    }

    fun saveFile() {
        val filePath = Controller.filePath
        if (filePath.isEmpty()) {
            showSaveDialog(mainWin, mainWin.buffer)
        } else {
            mainWin.updateStatusBar("Saving $filePath...")
            txtBuffer = textFromTextBuffer(mainWin.buffer).freeze()
            runOnBackgroundThread(staticCFunction(::runSaveFileTask))
        }
    }

    fun openFile() {
        showOpenDialog(mainWin)
    }

    fun exitProgram() {
        exitProcess(0)
    }
}

private fun runOpenFileTask(@Suppress("UNUSED_PARAMETER") userData: COpaquePointer?): COpaquePointer? {
    initRuntimeIfNeeded()
    _txtBuffer.value = readTextFile(_filePath.value).freeze()
    runOnUiThread(staticCFunction { _: COpaquePointer? ->
        Controller.mainWin.buffer.changeText(_txtBuffer.value)
        Controller.mainWin.title = "KPad - ${fileName(_filePath.value)}"
        Controller.mainWin.updateStatusBar("File opened")
        Controller.mainWin.resetFocus()
        FALSE
    })
    return null
}

private fun runSaveFileTask(@Suppress("UNUSED_PARAMETER") userData: COpaquePointer?): COpaquePointer? {
    initRuntimeIfNeeded()
    writeTextToFile(_filePath.value, _txtBuffer.value)
    runOnUiThread(staticCFunction { _: COpaquePointer? ->
        Controller.mainWin.title = "KPad - ${fileName(_filePath.value)}"
        Controller.mainWin.updateStatusBar("File saved")
        FALSE
    })
    return null
}

private fun openFileKeyPressed(@Suppress("UNUSED_PARAMETER") ptr: COpaquePointer) {
    initRuntimeIfNeeded()
    Controller.showOpenDialog(Controller.mainWin)
}

private fun saveFileKeyPressed(@Suppress("UNUSED_PARAMETER") ptr: COpaquePointer) {
    initRuntimeIfNeeded()
    val filePath = Controller.filePath
    if (filePath.isEmpty()) {
        Controller.showSaveDialog(Controller.mainWin, Controller.mainWin.buffer)
    } else {
        Controller.mainWin.updateStatusBar("Saving $filePath...")
        Controller.txtBuffer = Controller.textFromTextBuffer(Controller.mainWin.buffer).freeze()
        runOnBackgroundThread(staticCFunction(::runSaveFileTask))
    }
}

private fun newFileKeyPressed(@Suppress("UNUSED_PARAMETER") ptr: COpaquePointer) {
    initRuntimeIfNeeded()
    Controller.newFile()
}

private fun quitKeyPressed(@Suppress("UNUSED_PARAMETER") ptr: COpaquePointer) {
    initRuntimeIfNeeded()
    Controller.exitProgram()
}
